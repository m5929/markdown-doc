FROM openjdk:11.0.6-jdk
COPY build/libs/doc-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-Djdk.tls.client.protocols=TLSv1.2", "-jar", "/app.jar"]